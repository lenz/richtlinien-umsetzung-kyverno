apiVersion: kyverno.io/v1
kind: Policy
metadata:
  annotations:
    policies.kyverno.io/category: Pod Security Standards (Restricted)
    policies.kyverno.io/description: 'Privilege escalation, such as via set-user-ID
      or set-group-ID file mode, should not be allowed. This policy ensures the `allowPrivilegeEscalation`
      fields are either undefined or set to `false`.      '
    policies.kyverno.io/severity: medium
    policies.kyverno.io/subject: Pod
    policies.o4oe.de/bsi-requirement: SYS.1.6.A26
    policies.o4oe.de/release-version: 0.0.3-alpha
  labels:
    policies.o4oe.de/bsi-protection-requirement: standard
    policies.o4oe.de/category: should
  name: deny-privilege-escalation
spec:
  background: true
  rules:
  - match:
      resources:
        kinds:
        - Pod
    name: deny-privilege-escalation
    validate:
      message: 'Privilege escalation is disallowed. The fields spec.containers[*].securityContext.allowPrivilegeEscalation,
        and spec.initContainers[*].securityContext.allowPrivilegeEscalation must be
        undefined or set to `false`.        '
      pattern:
        spec:
          =(initContainers):
          - =(securityContext):
              =(allowPrivilegeEscalation): "false"
          containers:
          - =(securityContext):
              =(allowPrivilegeEscalation): "false"
  validationFailureAction: Enforce
---
apiVersion: kyverno.io/v1
kind: Policy
metadata:
  annotations:
    policies.kyverno.io/category: Pod Security Standards (Baseline)
    policies.kyverno.io/description: 'Capabilities permit privileged actions without
      giving full root access. Adding capabilities beyond the default set must not
      be allowed. This policy ensures users cannot add any additional capabilities
      to a Pod.  '
    policies.kyverno.io/severity: medium
    policies.kyverno.io/subject: Pod
    policies.o4oe.de/bsi-requirement: SYS.1.6.A31
    policies.o4oe.de/release-version: 0.0.3-alpha
  labels:
    policies.o4oe.de/bsi-protection-requirement: elevated
    policies.o4oe.de/category: should
  name: disallow-add-capabilities
spec:
  background: true
  rules:
  - match:
      resources:
        kinds:
        - Pod
    name: capabilities
    validate:
      message: 'Adding of additional capabilities beyond the default set is not allowed.
        The fields spec.containers[*].securityContext.capabilities.add and  spec.initContainers[*].securityContext.capabilities.add
        must be empty.          '
      pattern:
        spec:
          =(initContainers):
          - =(securityContext):
              =(capabilities):
                X(add): "null"
          containers:
          - =(securityContext):
              =(capabilities):
                X(add): "null"
  validationFailureAction: Enforce
---
apiVersion: kyverno.io/v1
kind: Policy
metadata:
  annotations:
    policies.kyverno.io/description: Containers should each use their own ServiceAccount
      to ensure authentication when communicating with Kubernetes system or other
      pods.
    policies.kyverno.io/subject: Pod
    policies.o4oe.de/bsi-requirement: SYS.1.6.A25
    policies.o4oe.de/release-version: 0.0.3-alpha
  labels:
    policies.o4oe.de/bsi-protection-requirement: standard
    policies.o4oe.de/category: should
  name: disallow-default-serviceaccount
spec:
  background: true
  rules:
  - match:
      resources:
        kinds:
        - Pod
    name: require-sa
    validate:
      message: serviceAccountName must be set to anything other than "default".
      pattern:
        spec:
          serviceAccountName: '!default'
  validationFailureAction: Enforce
---
apiVersion: kyverno.io/v1
kind: Policy
metadata:
  annotations:
    policies.kyverno.io/category: Pod Security Standards (Baseline)
    policies.kyverno.io/description: Host namespaces (Process ID namespace, Inter-Process
      Communication namespace, and network namespace) allow access to shared information
      and can be used to elevate privileges. Pods should not be allowed access to
      host namespaces.
    policies.kyverno.io/severity: medium
    policies.kyverno.io/subject: Pod
    policies.o4oe.de/bsi-requirement: SYS.1.6.A5
    policies.o4oe.de/release-version: 0.0.3-alpha
  labels:
    policies.o4oe.de/bsi-protection-requirement: basic
    policies.o4oe.de/category: must
  name: disallow-host-namespaces
spec:
  background: true
  rules:
  - match:
      resources:
        kinds:
        - Pod
    name: host-namespaces
    validate:
      message: Sharing the host namespaces is disallowed. The fields spec.hostNetwork,
        spec.hostIPC, and spec.hostPID must not be set to true.
      pattern:
        spec:
          =(hostIPC): "false"
          =(hostNetwork): "false"
          =(hostPID): "false"
  validationFailureAction: Enforce
---
apiVersion: kyverno.io/v1
kind: Policy
metadata:
  annotations:
    policies.kyverno.io/category: Pod Security Standards (Baseline)
    policies.kyverno.io/description: HostPath volumes let Pods use host directories
      and volumes in containers. Using host resources can be used to access shared
      data or escalate privileges and should not be allowed. This policy ensures no
      hostPath volumes are in use.
    policies.kyverno.io/severity: medium
    policies.kyverno.io/subject: Pod
    policies.o4oe.de/bsi-requirement: SYS.1.6.A5
    policies.o4oe.de/release-version: 0.0.3-alpha
  labels:
    policies.o4oe.de/bsi-protection-requirement: basic
    policies.o4oe.de/category: must
  name: disallow-host-path
spec:
  background: true
  rules:
  - match:
      resources:
        kinds:
        - Pod
    name: host-path
    validate:
      message: HostPath volumes are forbidden. The fields spec.volumes[*].hostPath
        must not be set.
      pattern:
        spec:
          =(volumes):
          - X(hostPath): "null"
  validationFailureAction: Enforce
---
apiVersion: kyverno.io/v1
kind: Policy
metadata:
  annotations:
    policies.kyverno.io/category: Pod Security Standards (Baseline)
    policies.kyverno.io/description: Access to host ports allows potential snooping
      of network traffic and should not be allowed, or at minimum restricted to a
      known list. This policy ensures the `hostPort` fields are empty.
    policies.kyverno.io/severity: medium
    policies.kyverno.io/subject: Pod
    policies.o4oe.de/bsi-requirement: SYS.1.6.A5
    policies.o4oe.de/release-version: 0.0.3-alpha
  labels:
    policies.o4oe.de/bsi-protection-requirement: basic
    policies.o4oe.de/category: must
  name: disallow-host-ports
spec:
  background: true
  rules:
  - match:
      resources:
        kinds:
        - Pod
    name: host-ports
    validate:
      message: Use of host ports is disallowed. The fields spec.containers[*].ports[*].hostPort
        and spec.initContainers[*].ports[*].hostPort must be empty.
      pattern:
        spec:
          =(initContainers):
          - =(ports):
            - X(hostPort): 0
          containers:
          - =(ports):
            - X(hostPort): 0
  validationFailureAction: Enforce
---
apiVersion: kyverno.io/v1
kind: Policy
metadata:
  annotations:
    policies.kyverno.io/category: Best Practices
    policies.kyverno.io/description: The ':latest' tag is mutable and can lead to
      unexpected errors if the image changes. A best practice is to use an immutable
      tag that maps to a specific version of an application Pod. This policy validates
      that the image specifies a tag and that it is not called `latest`.
    policies.kyverno.io/severity: medium
    policies.kyverno.io/subject: Pod
    policies.kyverno.io/title: Disallow Latest Tag
    policies.o4oe.de/bsi-requirement: SYS.1.6.A6
    policies.o4oe.de/release-version: 0.0.3-alpha
  labels:
    policies.o4oe.de/bsi-protection-requirement: basic
    policies.o4oe.de/category: must
  name: disallow-latest-tag
spec:
  background: true
  rules:
  - match:
      resources:
        kinds:
        - Pod
    name: require-image-tag
    validate:
      message: An image tag is required.
      pattern:
        spec:
          containers:
          - image: '*:*'
  - match:
      resources:
        kinds:
        - Pod
    name: validate-image-tag
    validate:
      message: Using a mutable image tag e.g. 'latest' is not allowed.
      pattern:
        spec:
          containers:
          - image: '!*:latest'
  validationFailureAction: Enforce
---
apiVersion: kyverno.io/v1
kind: Policy
metadata:
  annotations:
    policies.kyverno.io/category: Pod Security Standards (Baseline)
    policies.kyverno.io/description: Privileged mode disables most security mechanisms
      and must not be allowed.
    policies.kyverno.io/severity: medium
    policies.kyverno.io/subject: Pod
    policies.o4oe.de/bsi-requirement: SYS.1.6.A21
    policies.o4oe.de/release-version: 0.0.3-alpha
  labels:
    policies.o4oe.de/bsi-protection-requirement: standard
    policies.o4oe.de/category: must
  name: disallow-privileged-containers
spec:
  background: true
  rules:
  - match:
      resources:
        kinds:
        - Pod
    name: privileged-containers
    validate:
      message: Privileged mode is disallowed. The fields spec.containers[*].securityContext.privileged
        and spec.initContainers[*].securityContext.privileged must not be set to true.
      pattern:
        spec:
          =(initContainers):
          - =(securityContext):
              =(privileged): "false"
          containers:
          - =(securityContext):
              =(privileged): "false"
  validationFailureAction: Enforce
---
apiVersion: kyverno.io/v1
kind: Policy
metadata:
  annotations:
    policies.kyverno.io/category: Pod Security Standards (Baseline)
    policies.kyverno.io/description: SELinux options can be used to escalate privileges
      and should not be allowed. This policy ensures that the `seLinuxOptions` field
      is undefined.
    policies.kyverno.io/severity: medium
    policies.kyverno.io/subject: Pod
    policies.kyverno.io/title: Disallow SELinux
    policies.o4oe.de/bsi-requirement: SYS.1.6.A26
    policies.o4oe.de/release-version: 0.0.3-alpha
  labels:
    policies.o4oe.de/bsi-protection-requirement: standard
    policies.o4oe.de/category: should
  name: disallow-selinux-options
spec:
  background: true
  rules:
  - match:
      resources:
        kinds:
        - Pod
    name: seLinux
    validate:
      message: 'Setting custom SELinux options is disallowed. The fields spec.securityContext.seLinuxOptions,
        spec.containers[*].securityContext.seLinuxOptions, and spec.initContainers[*].securityContext.seLinuxOptions
        must be empty.          '
      pattern:
        spec:
          =(initContainers):
          - =(securityContext):
              X(seLinuxOptions): "null"
          =(securityContext):
            X(seLinuxOptions): "null"
          containers:
          - =(securityContext):
              X(seLinuxOptions): "null"
  validationFailureAction: Enforce
---
apiVersion: kyverno.io/v1
kind: Policy
metadata:
  annotations:
    policies.kyverno.io/category: Sample
    policies.kyverno.io/description: Sample policy that sets imagePullPolicy to "Always"
      when the "latest" tag is used.
    policies.kyverno.io/severity: medium
    policies.kyverno.io/subject: Pod
    policies.kyverno.io/title: Set imagePullPolicy
    policies.o4oe.de/bsi-requirement: SYS.1.6.A14
    policies.o4oe.de/release-version: 0.0.3-alpha
  labels:
    policies.o4oe.de/bsi-protection-requirement: standard
    policies.o4oe.de/category: should
  name: imagepullpolicy-always
spec:
  background: false
  rules:
  - match:
      resources:
        kinds:
        - Pod
    name: imagepullpolicy-always
    validate:
      message: The imagePullPolicy must be set to `Always` when the tag `latest` is
        used.
      pattern:
        spec:
          containers:
          - (image): '*:latest | !*:*'
            imagePullPolicy: Always
  validationFailureAction: Enforce
---
apiVersion: kyverno.io/v1
kind: Policy
metadata:
  annotations:
    policies.kyverno.io/category: Pod Security Standards (Baseline)
    policies.kyverno.io/description: 'The default /proc masks are set up to reduce
      attack surface and should be required. This policy ensures nothing but the default
      procMount can be specified.  '
    policies.kyverno.io/severity: medium
    policies.kyverno.io/subject: Pod
    policies.o4oe.de/bsi-requirement: SYS.1.6.A5
    policies.o4oe.de/release-version: 0.0.3-alpha
  labels:
    policies.o4oe.de/bsi-protection-requirement: basic
    policies.o4oe.de/category: must
  name: require-default-proc-mount
spec:
  background: true
  rules:
  - match:
      resources:
        kinds:
        - Pod
    name: check-proc-mount
    validate:
      message: 'Changing the proc mount from the default is not allowed. The fields
        spec.containers[*].securityContext.procMount and spec.initContainers[*].securityContext.procMount
        must not be changed  from `Default`.          '
      pattern:
        spec:
          =(initContainers):
          - =(securityContext):
              =(procMount): Default
          containers:
          - =(securityContext):
              =(procMount): Default
  validationFailureAction: Enforce
---
apiVersion: kyverno.io/v1
kind: Policy
metadata:
  annotations:
    policies.kyverno.io/category: Pod Security Standards (Restricted)
    policies.kyverno.io/description: 'Containers should be forbidden from running
      with a primary or supplementary GID less than 2000. This policy ensures the
      `runAsGroup`, `supplementalGroups`, and `fsGroup` fields are set to a number
      greater than 2000.  '
    policies.kyverno.io/minversion: 1.3.6
    policies.kyverno.io/severity: medium
    policies.kyverno.io/subject: Pod
    policies.o4oe.de/based-on: https://github.com/kyverno/policies/blob/7d77236d005af85fe5b10d5a31d5c97b7a61dfca/pod-security/restricted/require-non-root-groups/require-non-root-groups.yaml
    policies.o4oe.de/bsi-requirement: SYS.1.6.A26
    policies.o4oe.de/release-version: 0.0.3-alpha
  labels:
    policies.o4oe.de/bsi-protection-requirement: standard
    policies.o4oe.de/category: should
  name: require-gid-greater-2000
spec:
  background: true
  rules:
  - match:
      resources:
        kinds:
        - Pod
    name: check-runasgroup
    validate:
      message: "Running Pods or Containers with group IDs less than 2000 is disallowed.
        The fields\t spec.securityContext.runAsGroup, spec.containers[*].securityContext.runAsGroup,\t
        and spec.initContainers[*].securityContext.runAsGroup must be empty\t or greater
        than 2000.          "
      pattern:
        spec:
          =(initContainers):
          - =(securityContext):
              =(runAsGroup): '>2000'
          =(securityContext):
            =(runAsGroup): '>2000'
          containers:
          - =(securityContext):
              =(runAsGroup): '>2000'
  - match:
      resources:
        kinds:
        - Pod
    name: check-supplementalGroups
    validate:
      message: "Adding of supplemental group IDs less than 2000 is not allowed. The
        field\t spec.securityContext.supplementalGroups must not be defined or greater
        than 2000.        "
      pattern:
        spec:
          =(securityContext):
            =(supplementalGroups): '>2000'
  - match:
      resources:
        kinds:
        - Pod
    name: check-fsGroup
    validate:
      message: 'Changing to group ID less than 2000 is disallowed. The field spec.securityContext.fsGroup
        must be empty or less than 2000.          '
      pattern:
        spec:
          =(securityContext):
            =(fsGroup): '>2000'
  validationFailureAction: Enforce
---
apiVersion: kyverno.io/v1
kind: Policy
metadata:
  annotations:
    pod-policies.kyverno.io/autogen-controllers: DaemonSet,Deployment,StatefulSet
    policies.kyverno.io/category: Best Practices
    policies.kyverno.io/description: 'Liveness and readiness probes need to be configured
      to correctly manage a Pod''s  lifecycle during deployments, restarts, and upgrades.
      For each Pod, a periodic  `livenessProbe` is performed by the kubelet to determine
      if the Pod''s containers  are running or need to be restarted. A `readinessProbe`
      is used by Services  and Deployments to determine if the Pod is ready to receive
      network traffic. This policy validates that all containers have liveness and
      readiness probes by ensuring the `periodSeconds` field is greater than zero.      '
    policies.kyverno.io/severity: medium
    policies.kyverno.io/subject: Pod
    policies.kyverno.io/title: Require Pod Probes
    policies.o4oe.de/bsi-requirement: SYS.1.6.A27
    policies.o4oe.de/release-version: 0.0.3-alpha
  labels:
    policies.o4oe.de/bsi-protection-requirement: standard
    policies.o4oe.de/category: should
  name: require-health-and-liveness-check
spec:
  rules:
  - match:
      resources:
        kinds:
        - Pod
    name: validate-livenessProbe-readinessProbe
    validate:
      message: 'Liveness and readiness probes are required. spec.containers[*].livenessProbe.periodSeconds
        must be set to a value greater than 0. '
      pattern:
        spec:
          containers:
          - livenessProbe:
              periodSeconds: '>0'
            readinessProbe:
              periodSeconds: '>0'
  validationFailureAction: Enforce
---
apiVersion: kyverno.io/v1
kind: Policy
metadata:
  annotations:
    policies.kyverno.io/category: Multi-Tenancy
    policies.kyverno.io/description: 'As application workloads share cluster resources,
      it is important to limit resources  requested and consumed by each Pod. It is
      recommended to require resource requests and limits per Pod, especially for
      memory and CPU. If a Namespace level request or limit is specified,  defaults
      will automatically be applied to each Pod based on the LimitRange configuration.
      This policy validates that all containers have something specified for memory
      and CPU requests and memory limits.   '
    policies.kyverno.io/severity: medium
    policies.kyverno.io/subject: Pod
    policies.kyverno.io/title: Require Limits and Requests
    policies.o4oe.de/bsi-requirement: SYS.1.6.A16
    policies.o4oe.de/release-version: 0.0.3-alpha
  labels:
    policies.o4oe.de/bsi-protection-requirement: standard
    policies.o4oe.de/category: should
  name: require-requests-limits
spec:
  rules:
  - match:
      resources:
        kinds:
        - Pod
    name: validate-resources
    validate:
      message: CPU and memory resource requests and limits are required.
      pattern:
        spec:
          containers:
          - resources:
              limits:
                memory: ?*
              requests:
                cpu: ?*
                memory: ?*
  validationFailureAction: Enforce
---
apiVersion: kyverno.io/v1
kind: Policy
metadata:
  annotations:
    policies.kyverno.io/category: Best Practices
    policies.kyverno.io/description: A read-only root file system helps to enforce
      an immutable infrastructure strategy;  the container only needs to write on
      the mounted volume that persists the state.  An immutable root filesystem can
      also prevent malicious binaries from writing to the  host system.
    policies.kyverno.io/severity: medium
    policies.kyverno.io/subject: Pod
    policies.kyverno.io/title: Require Read-Only Root Filesystem
    policies.o4oe.de/bsi-requirement: SYS.1.6.A21
    policies.o4oe.de/release-version: 0.0.3-alpha
  labels:
    policies.o4oe.de/bsi-protection-requirement: standard
    policies.o4oe.de/category: should
  name: require-ro-rootfs
spec:
  rules:
  - match:
      resources:
        kinds:
        - Pod
    name: validate-readOnlyRootFilesystem
    validate:
      message: Root filesystem must be read-only.
      pattern:
        spec:
          containers:
          - securityContext:
              readOnlyRootFilesystem: true
  validationFailureAction: Enforce
---
apiVersion: kyverno.io/v1
kind: Policy
metadata:
  annotations:
    policies.kyverno.io/category: Pod Security Standards (Restricted)
    policies.kyverno.io/description: Containers must be required to run as non-root
      users. This policy ensures `runAsNonRoot` is set to `true`.
    policies.kyverno.io/severity: medium
    policies.kyverno.io/subject: Pod
    policies.o4oe.de/bsi-requirement: SYS.1.6.A26
    policies.o4oe.de/release-version: 0.0.3-alpha
  labels:
    policies.o4oe.de/bsi-protection-requirement: standard
    policies.o4oe.de/category: should
  name: require-run-as-non-root
spec:
  background: true
  rules:
  - match:
      any:
      - resources:
          kinds:
          - Pod
    name: run-as-non-root
    validate:
      message: Running as root is not allowed. Either the field spec.securityContext.runAsNonRoot
        must be set to `true`, or the fields spec.containers[*].securityContext.runAsNonRoot,
        spec.initContainers[*].securityContext.runAsNonRoot, and spec.ephemeralContainers[*].securityContext.runAsNonRoot
        must be set to `true`.
      pattern:
        spec:
          =(ephemeralContainers):
          - securityContext:
              runAsNonRoot: true
          =(initContainers):
          - securityContext:
              runAsNonRoot: true
          containers:
          - securityContext:
              runAsNonRoot: true
  validationFailureAction: Enforce
---
apiVersion: kyverno.io/v1
kind: Policy
metadata:
  annotations:
    policies.kyverno.io/description: Containers should be forbidden from running with
      a UID less than or equal 2000.
    policies.o4oe.de/based-on: https://raw.githubusercontent.com/kyverno/policies/main/other/restrict_usergroup_fsgroup_id/restrict_usergroup_fsgroup_id.yaml
    policies.o4oe.de/bsi-requirement: SYS.1.6.A26
    policies.o4oe.de/release-version: 0.0.3-alpha
  labels:
    policies.o4oe.de/bsi-protection-requirement: standard
    policies.o4oe.de/category: should
  name: require-uid-greater-2000
spec:
  background: true
  rules:
  - match:
      resources:
        kinds:
        - Pod
    name: validate-userid
    validate:
      message: User ID should be higher than 2000. (spec.containers.securityContext.runAsUser)
      pattern:
        spec:
          containers:
          - securityContext:
              runAsUser: '>2000'
  validationFailureAction: Enforce
---
apiVersion: kyverno.io/v1
kind: Policy
metadata:
  annotations:
    policies.kyverno.io/description: |-
      Two distinct workloads should not share a UID so that in a multitenant environment, applications  from different projects never run as the same user ID. When using persistent storage,  any files created by applications will also have different ownership in the file system.
      Running processes for applications as different user IDs means that if a security  vulnerability were ever discovered in the underlying container runtime, and an application  were able to break out of the container to the host, they would not be able to interact  with processes owned by other users, or from other applications, in other projects.
    policies.kyverno.io/minversion: 1.3.6
    policies.o4oe.de/bsi-requirement: APP.4.4.A4
    policies.o4oe.de/release-version: 0.0.3-alpha
  labels:
    policies.o4oe.de/category: must
  name: require-unique-uid-per-workload
spec:
  background: false
  rules:
  - context:
    - apiCall:
        jmesPath: items[?@.metadata.ownerReferences == false || metadata.ownerReferences[?uid
          != '{{ request.object.metadata.keys(@).contains(@, 'ownerReferences') &&
          request.object.metadata.ownerReferences[0].uid }}']].spec.containers[].securityContext.to_string(runAsUser)
        urlPath: /api/v1/pods
      name: uidsAllPodsExceptSameOwnerAsRequestObject
    match:
      resources:
        kinds:
        - Pod
    name: require-unique-uid
    preconditions:
    - key: '{{ request.operation }}'
      operator: Equals
      value: CREATE
    validate:
      deny:
        conditions:
        - key: '{{ request.object.spec.containers[].securityContext.to_string(runAsUser)
            }}'
          operator: In
          value: '{{ uidsAllPodsExceptSameOwnerAsRequestObject }}'
      message: Only cluster-unique UIDs are allowed
  validationFailureAction: Enforce
---
apiVersion: kyverno.io/v1
kind: Policy
metadata:
  annotations:
    policies.kyverno.io/category: Pod Security Standards (Baseline)
    policies.kyverno.io/description: On supported hosts, the 'runtime/default' AppArmor
      profile is applied by default.  The default policy should prevent overriding
      or disabling the policy, or restrict  overrides to an allowed set of profiles.
      This policy ensures Pods do not specify any other AppArmor profiles than `runtime/default`.
    policies.kyverno.io/minversion: 1.3.0
    policies.kyverno.io/severity: medium
    policies.kyverno.io/subject: Pod, Annotation
    policies.o4oe.de/bsi-requirement: SYS.1.6.A26
    policies.o4oe.de/release-version: 0.0.3-alpha
  labels:
    policies.o4oe.de/bsi-protection-requirement: standard
    policies.o4oe.de/category: should
  name: restrict-apparmor
spec:
  background: true
  rules:
  - match:
      resources:
        kinds:
        - Pod
    name: app-armor
    validate:
      message: 'Specifying other AppArmor profiles is disallowed. The annotation container.apparmor.security.beta.kubernetes.io
        must not be defined, or must not be set to anything other than `runtime/default`.          '
      pattern:
        metadata:
          =(annotations):
            =(container.apparmor.security.beta.kubernetes.io/*): runtime/default
  validationFailureAction: Enforce
---
apiVersion: kyverno.io/v1
kind: Policy
metadata:
  annotations:
    policies.ig-bvc.de/cve: 2020-8554
    policies.kyverno.io/category: Best Practices
    policies.kyverno.io/description: "Service externalIPs can be used for a MITM attack
      (CVE-2020-8554). Restrict externalIPs or limit to a known set of addresses.
      See: https://github.com/kyverno/kyverno/issues/1367. This policy validates that
      the `externalIPs` field is not set on a Service.  \nThis Policy applies to all
      types of kind \"Service\", including ClusterIP and LoadBalancer."
    policies.kyverno.io/severity: medium
    policies.kyverno.io/subject: Service
    policies.kyverno.io/title: Restrict External IPs
    policies.o4oe.de/release-version: 0.0.3-alpha
  name: restrict-external-ips
spec:
  rules:
  - match:
      resources:
        kinds:
        - Service
    name: check-ips
    validate:
      message: externalIPs are not allowed.
      pattern:
        spec:
          X(externalIPs): "null"
  validationFailureAction: Enforce
---
apiVersion: kyverno.io/v1
kind: Policy
metadata:
  annotations:
    policies.kyverno.io/category: Best Practices
    policies.kyverno.io/description: Images from unknown registries may not be scanned
      and secured.  Requiring use of known registries helps reduce threat exposure.
    policies.kyverno.io/minversion: 1.3.0
    policies.kyverno.io/severity: medium
    policies.kyverno.io/subject: Pod
    policies.kyverno.io/title: Restrict Image Registries
    policies.o4oe.de/bsi-requirement: SYS.1.6.A6
    policies.o4oe.de/release-version: 0.0.3-alpha
  labels:
    policies.o4oe.de/bsi-protection-requirement: basic
    policies.o4oe.de/category: must
  name: restrict-image-registries
spec:
  background: false
  rules:
  - match:
      resources:
        kinds:
        - Pod
    name: validate-registries
    validate:
      message: Unknown image registry.
      pattern:
        spec:
          containers:
          - image: registry.o4oe.de/*
  validationFailureAction: Enforce
---
apiVersion: kyverno.io/v1
kind: Policy
metadata:
  annotations:
    policies.kyverno.io/category: Pod Security Standards (Baseline)
    policies.kyverno.io/description: 'Sysctls can disable security mechanisms or affect
      all containers on a host, and should be disallowed except for an allowed "safe"
      subset. A sysctl is considered safe if it is namespaced in the container or
      the Pod, and it is isolated from other Pods or processes on the same Node. This
      policy ensures that only those "safe" subsets can be specified in a Pod. '
    policies.kyverno.io/severity: medium
    policies.kyverno.io/subject: Pod
    policies.o4oe.de/bsi-requirement: SYS.1.6.A31
    policies.o4oe.de/release-version: 0.0.3-alpha
  labels:
    policies.o4oe.de/bsi-protection-requirement: elevated
    policies.o4oe.de/category: should
  name: restrict-sysctls
spec:
  background: true
  rules:
  - match:
      resources:
        kinds:
        - Pod
    name: sysctls
    validate:
      message: 'Setting additional sysctls above the allowed type is disallowed. The
        field spec.securityContext.sysctls must not use any other names than ''kernel.shm_rmid_forced'',
        ''net.ipv4.ip_local_port_range'', ''net.ipv4.tcp_syncookies'' and ''net.ipv4.ping_group_range''.          '
      pattern:
        spec:
          =(securityContext):
            =(sysctls):
            - name: kernel.shm_rmid_forced | net.ipv4.ip_local_port_range | net.ipv4.tcp_syncookies
                | net.ipv4.ping_group_range
              value: ?*
  validationFailureAction: Enforce
